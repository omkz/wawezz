var http = require('blaast/simple-http');
var rssparser = require('htmlparser');
var _ = require('underscore');
var sys = require('sys');
var jsdom = require('jsdom');
var util = require('util');
var scaling = new (require('blaast/scaling').Scaling)();

var debug = true;

var url = "http://causewaymall.com/shop/rss.php/";

app.message(function(client, action, data) {
    if (action === 'getList') {
        var handler = new rssparser.RssHandler(function (error, dom) {
            if (error) {
                console.log(error);
            } else {
                if (debug) {
                    sys.puts('Raw Data: ' + sys.inspect(dom, false, null));
                }
                _.each(dom.items, function(param) {
                    //console.log('Id: ' + param.id);
                    console.log('Title: ' + param.title);
//                    console.log('Pub Date: ' + param.pubDate);
                    console.log('Link: ' + param.link);
                    console.log('########################################');

                    client.msg('getList', {
                        text : { title_ : param.title, link_:param.link}
                    });
                });
            }
        });

        var parser = new rssparser.Parser(handler);

        http.get(url, {
            ok: function(data) {
                parser.parseComplete(data);
            },
            error: function(err) {
                console.log('err: ' + err);
            }
        });

    }

    if (action === 'parse') {
        var urlImage = data.textUrl;
        console.log('Data url => ' + urlImage);
        jsdom.env(urlImage,
            [
                'http://code.jquery.com/jquery-1.5.min.js'
            ],
            function(errors, window) {
                console.log("Url Image : =>  " + window.$(".main1 img").attr("src"));
                var image = window.$(".main1 img").attr("src");
                window.close();
                client.msg('parse', {
                    text :{uri : image}
                });
            });
    }

});

app.setResourceHandler(function(request, response) {

    app.debug('Client requested resource-id=' + request.id);

    function sendReply(response, error, imageType, data) {
        if (error) {
            app.warn('Failed to load image: ' + error);
            response.failed();
        } else {
            app.debug('Loaded image.');
            response.reply(imageType, data);
        }
    }

    if (request.id !== null) {
        scaling.scale(request.id, request.display_width, request.display_height, 'image/jpeg',
            function(err, data) {
                sendReply(response, err, 'image/jpeg', data);
            }
        );
    }
});
